This repo is a basic setup for serving an index.html page with some basic javascript.
It uses 'LiteServer' to watch for changes and serve files.
It has configuration options which can be configured in bs-config.json in the root of the project.

To use:
1. clone repo, run "npm install" to install LiteServer.
2. Type "npm run dev" to start server".